# Sprint 4

<img src="/Imagens_Sprint4/logo.jpg"/>

## :computer: Proposta

Na Sprint 4,realizamos a finalização do sistema Planning Pokker , na qual foi realizada a adição do timer para a realização de cada votação das atividades adicionadas, o sistema tambem ira possuir um sistema de salvar o resumo da restrospectiva da votação e finalizando os detalhes dos layouts para a melhor funcionamento e entendimento ao cliente.

##  :chart_with_upwards_trend: Meta da Sprint

Na Sprint 4 organizamos como meta a finalização dos últimos detalhes do Projeto e também a inserção dos requisitos finais para a funcionalidade da votação se tornar mais clara e mais descritiva do que foi realizado a partir dela, sendo inserido a exibição de voto, exportação de itens votados, tempo de votação e salvamento da pontuação final do item votado.

## :dart: Detalhes da Sprint:

**Implementações do back-end:**

- Detalhes do layout para a facilitação do uso entre Desenvolvedor  e plataforma;



**Implementações Front-End:**
-  Adicionação o Timer para a votação Planning Pokker para que seja feito de maneira sucinta e sem muito tempo de espera.
- Salvar resumo da retrospectiva para ser revisado com toda equipe o que foi decidido para sua montagem.



## Apresentação da Sprint
- Em desenvolvimento.



## Wireframes:

- Em desenvolvimento



## :department_store: Proposta para a apresentação final:
- Ofecer ao cliente uma home page com um acesso de bastante simplicidade e com melhor facilidade para melhor contribuição para realização de atividades gerais e votação dinamica para a melhor compreensão de todos sobre suas entregas.


